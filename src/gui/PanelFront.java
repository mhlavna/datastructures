/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import datastructures.fronts.EFront;
import datastructures.fronts.IFront;
import java.text.DecimalFormat;

/**
 *
 * @author Michal Varga
 */
public class PanelFront extends javax.swing.JPanel {

    private static final int PUSH = 0;
    private static final int POP = 1;
    private static final int PEEK = 2;
    private static final int SIZE = 5;
    private static final int CLEAR = 6; 
    
    private IFront aFront;
    private IEditData aEditData;   
    
    private void setEnabledComponents(boolean paValue) {
        btnPush.setEnabled(paValue);
        btnPop.setEnabled(paValue);
        btnPeek.setEnabled(paValue);
        btnSize.setEnabled(paValue);
        btnClear.setEnabled(paValue);
        btnTest.setEnabled(paValue);
        btnTestPush.setEnabled(paValue);
        btnTestPop.setEnabled(paValue);
    }
    
    /**
     * Creates new form PanelStructure
     */
    public PanelFront() {
        initComponents();
        
        aFront = null;
        aEditData = null;
        
        setEnabledComponents(false);
        
        panelOperations.registerOperation(PUSH, "Push");
        panelOperations.registerOperation(POP, "Pop");
        panelOperations.registerOperation(PEEK, "Peek");
        panelOperations.registerOperation(SIZE, "Size");
        panelOperations.registerOperation(CLEAR, "Clear");
    }
    
    public void init(IFront paFront, IEditData paEditData) {
        aFront = paFront;
        aEditData = paEditData;
        
        Routines.listFrontElements(aFront, listElements);
        
        setEnabledComponents(true);        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelOperations = new gui.PanelOperations();
        toolsFront = new javax.swing.JToolBar();
        lblOperationsFront = new javax.swing.JLabel();
        btnPush = new javax.swing.JButton();
        btnPop = new javax.swing.JButton();
        btnPeek = new javax.swing.JButton();
        btnSize = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        toolsTests = new javax.swing.JToolBar();
        lblOperationsTest = new javax.swing.JLabel();
        btnTestPush = new javax.swing.JButton();
        btnTestPop = new javax.swing.JButton();
        btnTest = new javax.swing.JButton();
        lblElements = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listElements = new javax.swing.JList();

        toolsFront.setRollover(true);

        lblOperationsFront.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        lblOperationsFront.setText("Operations:");
        toolsFront.add(lblOperationsFront);

        btnPush.setText("Push");
        btnPush.setFocusable(false);
        btnPush.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPush.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPush.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPushActionPerformed(evt);
            }
        });
        toolsFront.add(btnPush);

        btnPop.setText("Pop");
        btnPop.setFocusable(false);
        btnPop.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPop.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPopActionPerformed(evt);
            }
        });
        toolsFront.add(btnPop);

        btnPeek.setText("Peek");
        btnPeek.setFocusable(false);
        btnPeek.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPeek.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPeek.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPeekActionPerformed(evt);
            }
        });
        toolsFront.add(btnPeek);

        btnSize.setText("Size");
        btnSize.setFocusable(false);
        btnSize.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSize.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSizeActionPerformed(evt);
            }
        });
        toolsFront.add(btnSize);

        btnClear.setText("Clear");
        btnClear.setFocusable(false);
        btnClear.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnClear.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });
        toolsFront.add(btnClear);

        toolsTests.setRollover(true);

        lblOperationsTest.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        lblOperationsTest.setText("Testing:");
        toolsTests.add(lblOperationsTest);

        btnTestPush.setText("Push");
        btnTestPush.setFocusable(false);
        btnTestPush.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnTestPush.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnTestPush.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestPushActionPerformed(evt);
            }
        });
        toolsTests.add(btnTestPush);

        btnTestPop.setText("Pop");
        btnTestPop.setFocusable(false);
        btnTestPop.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnTestPop.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnTestPop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestPopActionPerformed(evt);
            }
        });
        toolsTests.add(btnTestPop);

        btnTest.setText("Complex test");
        btnTest.setFocusable(false);
        btnTest.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnTest.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnTest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestActionPerformed(evt);
            }
        });
        toolsTests.add(btnTest);

        lblElements.setText("Elements");

        jScrollPane1.setViewportView(listElements);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelOperations, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblElements)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE))
                .addContainerGap())
            .addComponent(toolsFront, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(toolsTests, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(toolsFront, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(toolsTests, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblElements)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelOperations, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPushActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPushActionPerformed
        try {
            Object data = aEditData.createData();
            if (data != null) {  
                panelOperations.start(PUSH);
                aFront.push(data);
                panelOperations.finish(PUSH,true);
            }
            else
               panelOperations.abort(PUSH,true);
        } catch (EFront ex) {
            panelOperations.except(PUSH,ex,true);
        }
        
        Routines.listFrontElements(aFront, listElements); 
    }//GEN-LAST:event_btnPushActionPerformed

    private void btnPopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPopActionPerformed
        try {
            if(aFront.isEmpty()) {
                panelOperations.log("Front is empty!");
                return;
            }
            
            panelOperations.start(POP);
            Object data = aFront.pop();
            panelOperations.finish(POP, data.toString(), true);
        } catch (EFront ex) {
            panelOperations.except(POP,ex,true);
        }
        
        Routines.listFrontElements(aFront, listElements); 
    }//GEN-LAST:event_btnPopActionPerformed

    private void btnPeekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPeekActionPerformed
        try {
            if(aFront.isEmpty()) {
                panelOperations.log("Front is empty!");
                return;
            }
            
            panelOperations.start(PEEK);
            Object data = aFront.peek();
            panelOperations.finish(PEEK, data.toString(), true);
        } catch (EFront ex) {
            panelOperations.except(PEEK,ex,true);
        }
    }//GEN-LAST:event_btnPeekActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        try { 
            panelOperations.start(CLEAR);
            aFront.clear();
            panelOperations.finish(CLEAR,true);
            Routines.listFrontElements(aFront, listElements);
        } catch (EFront ex) {
            panelOperations.except(CLEAR,ex,true);
        }
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnTestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestActionPerformed
        try {                        
            int iterations = Routines.getIntDialog("Number of iterations");
            if (iterations != -1) {
                int countPush = 0;
                int countPop = 0;
                
                panelOperations.start(PUSH);                                               
                panelOperations.start(POP);
                
                panelOperations.pause(PUSH);                                               
                panelOperations.pause(POP);
                
                for(int i = 0; i < iterations; i++) {
                                                            
                    int operation = Routines.random(aFront.isEmpty() ? 1 : 2);
                    
                    switch(operation) {
                        case 0:
                            countPush++;
                            panelOperations.resume(PUSH);
                            aFront.push(aEditData.createRandomData());                            
                            panelOperations.pause(PUSH);
                            break;
                        case 1:
                            countPop++;
                            panelOperations.resume(POP);
                            aFront.pop();
                            panelOperations.pause(POP);
                            break;
                    }                                                                            
                }                
                                
                long timeAdd = panelOperations.finish(PUSH, true); 
                long timeInsert = panelOperations.finish(POP, true);
                
                DecimalFormat decFormat = new DecimalFormat( "# ###0.0000" );
                
                panelOperations.log("Number of push operations: " + countPush + " average time: " + decFormat.format(timeAdd/(double)countPush) + PanelOperations.UNITS);
                panelOperations.log("Number of pop operations: " + countPop + " average time: " + decFormat.format(timeInsert/(double)countPop) + PanelOperations.UNITS);
            }
            else {
                panelOperations.abort(PUSH,false);
                panelOperations.abort(POP,false);
            }
        } catch (EFront ex) {
            panelOperations.except(PUSH,ex,false);
            panelOperations.except(POP,ex,false);
        }     
         
         Routines.listFrontElements(aFront, listElements);
    }//GEN-LAST:event_btnTestActionPerformed

    private void btnTestPopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestPopActionPerformed
        try {            
            int iterations = Routines.getIntDialog("Number of iterations");
            if (iterations != -1) {
                panelOperations.start(POP);                
                
                iterations = iterations < aFront.size() ? iterations : aFront.size();
                
                for(int i = 0; i < iterations; i++)                     
                    aFront.pop();                
                                
                long time = panelOperations.finish(POP,true); 
                panelOperations.log("Average time: " + (time/(double)iterations) + PanelOperations.UNITS);
            }
            else
                 panelOperations.abort(POP,true);
        } catch (EFront ex) {
            panelOperations.except(POP,ex,true);
        }     
         
         Routines.listFrontElements(aFront, listElements);
    }//GEN-LAST:event_btnTestPopActionPerformed

    private void btnTestPushActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestPushActionPerformed
        try {            
            int iterations = Routines.getIntDialog("Number of iterations");
            if (iterations != -1) {
                panelOperations.start(PUSH);                
                
                for(int i = 0; i < iterations; i++) {
                    aFront.push(aEditData.createRandomData());
                }                
                                
                long time = panelOperations.finish(PUSH,true); 
                panelOperations.log("Average time: " + (time/(double)iterations) + PanelOperations.UNITS);                
            }
            else
                 panelOperations.abort(PUSH,true);
        } catch (EFront ex) {
            panelOperations.except(PUSH,ex,true);
        }     
         
         Routines.listFrontElements(aFront, listElements);
    }//GEN-LAST:event_btnTestPushActionPerformed

    private void btnSizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSizeActionPerformed
        try { 
            panelOperations.start(SIZE);
            int size = aFront.size();        
            panelOperations.finish(SIZE,Integer.toString(size),true);
        } catch (EFront ex) {
            panelOperations.except(SIZE,ex,true);
        }
    }//GEN-LAST:event_btnSizeActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnPeek;
    private javax.swing.JButton btnPop;
    private javax.swing.JButton btnPush;
    private javax.swing.JButton btnSize;
    private javax.swing.JButton btnTest;
    private javax.swing.JButton btnTestPop;
    private javax.swing.JButton btnTestPush;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblElements;
    private javax.swing.JLabel lblOperationsFront;
    private javax.swing.JLabel lblOperationsTest;
    private javax.swing.JList listElements;
    private gui.PanelOperations panelOperations;
    private javax.swing.JToolBar toolsFront;
    private javax.swing.JToolBar toolsTests;
    // End of variables declaration//GEN-END:variables
}
