/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.fronts.stack;

import datastructures.fronts.AbstractFront;
import datastructures.fronts.EFront;
import datastructures.lists.EList;
import datastructures.lists.LinkedList.LinkedList;

/**
 *
 * @author hlavna2
 */
public class Stack<E> extends AbstractFront<E> {

    public Stack() {
        super(new LinkedList<E>());
    }

    @Override
    public E pop() throws EFront {
        try {
            return aList.deleteFromIndex(this.size() - 1);
        } catch (EList ex) {
            throw new EFront(ex.getMessage());
        }
    }

    @Override
    public E peek() throws EFront {
        try {
            return aList.get(this.size() - 1);
        } catch (EList ex) {
            throw new EFront(ex.getMessage());
        }
    }

    @Override
    public void push(E paElement) throws EFront {
        try {
            aList.add(paElement);
        } catch (EList ex) {
            throw new EFront(ex.getMessage());
        }
    }
}
