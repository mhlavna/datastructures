/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.fronts.front;

import datastructures.fronts.AbstractFront;
import datastructures.fronts.EFront;
import datastructures.lists.EList;
import datastructures.lists.LinkedList.LinkedList;

/**
 *
 * @author hlavna2
 */
public class Front<E> extends AbstractFront<E> {

    public Front() {
        super(new LinkedList<E>());
    }

    @Override
    public void push(E paElement) throws EFront {
        try {
            aList.add(paElement);
        } catch (EList ex) {
            throw new EFront(ex.getMessage());
        }
    }

    @Override
    public E pop() throws EFront {
        try {
            return aList.deleteFromIndex(0);
        } catch (EList ex) {
            throw new EFront(ex.getMessage());
        }
    }

    @Override
    public E peek() throws EFront {
        try {
            return aList.get(0);
        } catch (EList ex) {
            throw new EFront(ex.getMessage());
        }
    }
}
