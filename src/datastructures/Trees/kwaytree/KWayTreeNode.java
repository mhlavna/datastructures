/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.Trees.kwaytree;

import datastructures.lists.ArrayList.ArrayList;
import datastructures.lists.EList;
import datastructures.trees.ETree;
import datastructures.trees.ITreeNode;

public class KWayTreeNode<E> implements ITreeNode<E> {

    private E aData;
    private ITreeNode<E> aParent;
    private ArrayList<ITreeNode<E>> aSons;
    private int aCardinality;

    public KWayTreeNode(E aData, int k) {
        this.aData = aData;
        this.aCardinality = k;
        this.aSons = new ArrayList<>();
        this.aParent = null;
    }

    @Override
    public E getData() throws ETree {
        return aData;
    }

    @Override
    public ITreeNode<E> getParent() throws ETree {
        return aParent;
    }

    @Override
    public void setParent(ITreeNode<E> paParent) throws ETree {
        aParent = paParent;

    }

    @Override
    public ITreeNode<E> getBrother(int paOrder) throws ETree {
        return isRoot() ? null : aParent.getChild(paOrder);
    }

    @Override
    public ITreeNode<E> getChild(int paOrder) throws ETree {
        if (paOrder <= this.degree() && paOrder >= 0) {
            try {
                return aSons.get(paOrder);
            } catch (EList ex) {
                throw new ETree(ex.getMessage());
            }
        }
        throw new ETree("Index out of bounds");


    }

    @Override
    public void addChild(ITreeNode<E> paChild) throws ETree {
        if (this.degree() < this.aCardinality) {
            try {
                this.aSons.add(paChild);
            } catch (EList ex) {
                throw new ETree(ex.getMessage());
            }
        } else {
            throw new ETree("Max count of sons reached, cannot add more childs");
        }
    }

    @Override
    public void deleteChild(ITreeNode<E> paChild) throws ETree {
        try {
            this.aSons.delete(paChild);
        } catch (EList ex) {
            throw new ETree(ex.getMessage());
        }
    }

    @Override
    public boolean isRoot() throws ETree {
        return aParent == null;
    }

    @Override
    public boolean isLeaf() throws ETree {
        return degree() == 0;
    }

    @Override
    public int degree() throws ETree {
        try {
            return this.aSons.size();
        } catch (EList ex) {
            throw new ETree(ex.getMessage());
        }
    }

    @Override
    public int level() throws ETree {
        if (aParent == null) {
            return 0;
        }

        return aParent.level() + 1;
    }

    public int levelUsingCycle() throws ETree {
        ITreeNode<E> tmpParent = aParent;
        int iterator = 0;
        while (tmpParent != null) {
            iterator++;
            tmpParent = tmpParent.getParent();
        }
        return iterator;
    }

    @Override
    public String toString() {
        try {
            String output = "KWayTreeNode:\n";
            output += "Degree: " + degree() + "\n";
            output += "Level: " + level() + "\n";
            output += "Leaf: " + (isLeaf() ? "True" : "false") + "\n";
            output += "Root: " + (isRoot() ? "True" : "false") + "\n";
            if (aData != null) {
                output += "Data to string: " + aData.toString();
            }
            return output;
        } catch (ETree ex) {
            return "Error retrieving string reprezentation of KEWayTreeNode";
        }

    }
}
