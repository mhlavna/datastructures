/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.trees.kwaytree;

import datastructures.lists.IList;
import datastructures.trees.ETree;
import datastructures.trees.ITree;
import datastructures.trees.ITreeNode;
import datastructures.Trees.kwaytree.KWayTreeNode;
import datastructures.fronts.EFront;
import datastructures.fronts.front.Front;
import datastructures.lists.EList;
import datastructures.lists.LinkedList.LinkedList;

public class KWayTree<E> implements ITree<E> {

    private KWayTreeNode<E> aRoot;
    private int aCardinality;

    public KWayTree(int aCardinality) {
        this.aCardinality = aCardinality;
        aRoot = null;
    }

    @Override
    public boolean isEmpty() throws ETree {
        return aRoot == null;
    }

    @Override
    public int size() throws ETree {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clear() throws ETree {
        aRoot = null;
    }

    @Override
    public void insertRoot(E paElement) throws ETree {
        KWayTreeNode<E> kWayTreeNode = new KWayTreeNode<>(paElement, aCardinality);
        if (!isEmpty()) {
            kWayTreeNode.addChild(aRoot);
            aRoot.setParent(kWayTreeNode);

        }

        aRoot = kWayTreeNode;


    }

    @Override
    public ITreeNode<E> getRoot() throws ETree {
        return aRoot;
    }

    @Override
    public E deleteRoot() throws ETree {


        if (isEmpty()) {
            return null;
        }

        KWayTreeNode<E> oldroot = aRoot;
        // implementacia zavisi od aplikacie da sa použit viac pristupou napr. nastav najstarsieho syna ako root a zlikviduj vsetko ostatne, nastav najstarsieho syna ako root a presun jeho bratov pod neho
        //aRoot = null;

        if (oldroot.degree() == 0) {
            aRoot = null;
        } else {
            KWayTreeNode<E> oldestChild = (KWayTreeNode<E>) oldroot.getChild(0);
            aRoot = oldestChild;
            oldroot.deleteChild(oldestChild);

            while (oldestChild.degree() < aCardinality && oldroot.degree() > 0) {
                KWayTreeNode<E> brother = (KWayTreeNode<E>) oldroot.getChild(0);
                oldroot.deleteChild(brother);
                brother.setParent(oldestChild);
                oldestChild.addChild(brother);
            }
        }

        return oldroot.getData();
    }

    @Override
    public void insertLeaf(E paElement, ITreeNode<E> paParent) throws ETree {
        KWayTreeNode<E> node = new KWayTreeNode<>(paElement, aCardinality);
        node.setParent(paParent);
        paParent.addChild(node);
    }

    @Override
    public E deleteChild(ITreeNode<E> paParent, int paOrder) throws ETree {
        KWayTreeNode<E> node = (KWayTreeNode<E>) paParent.getChild(paOrder);
        paParent.deleteChild(node);
        return node.getData();
    }

    @Override
    public boolean isComplete() throws ETree {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isBalanced() throws ETree {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public IList<ITreeNode<E>> preOrder() throws ETree {
        LinkedList<ITreeNode<E>> linkedList = new LinkedList<>();
        preOrderR(aRoot, linkedList);
        return linkedList;
    }

    @Override
    public IList<ITreeNode<E>> postOrder() throws ETree {
        LinkedList<ITreeNode<E>> linkedList = new LinkedList<>();
        postOrderR(aRoot, linkedList);
        return linkedList;
    }

    private void preOrderR(ITreeNode<E> node, LinkedList<ITreeNode<E>> list) throws ETree {
        try {
            list.add(node);
            for (int i = 1; i < node.degree(); i++) {
                preOrderR((KWayTreeNode<E>) node.getChild(i), list);
            }
        } catch (EList ex) {
            throw new ETree("Error while levelOrder caused by: " + ex.getMessage());
        }

    }

    private void postOrderR(ITreeNode<E> node, LinkedList<ITreeNode<E>> list) throws ETree {
        try {

            for (int i = 0; i < node.degree(); i++) {
                preOrderR((KWayTreeNode<E>) node.getChild(i), list);
            }
            list.add(node);
        } catch (EList ex) {
            throw new ETree("Error while levelOrder caused by: " + ex.getMessage());
        }

    }

    @Override
    public IList<ITreeNode<E>> levelOrder() throws ETree {
        LinkedList<ITreeNode<E>> linkedList = new LinkedList<>();
        Front<KWayTreeNode> front = new Front<>();
        try {
            front.push(aRoot);

            while (!front.isEmpty()) {
                KWayTreeNode pop = front.pop();
                linkedList.add(pop);
                for (int i = 0; i < pop.degree(); i++) {
                    front.push((KWayTreeNode) pop.getChild(i));
                }

            }

        } catch (EFront | EList ex) {
            throw new ETree("Error while levelOrder caused by: " + ex.getMessage());
        }
        return linkedList;

    }
}
