/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.priorityFronts;

import datastructures.lists.EList;
import datastructures.lists.LinkedList.LinkedList;
import datastructures.lists.LinkedList.SortedLinkedList;
import java.util.Iterator;

/**
 *
 * @author Martin Hlavňa
 */
public class PriorityFront<E> implements IPriorityFront<E> {

    private SortedLinkedList<priorityNode<E>> aShort;
    private LinkedList<priorityNode<E>> aLong;
    private int aPmax;
    private boolean isShortFull;
    private int aCount;

    @Override
    public void push(E paElement, int paPriority) throws EPriorityFront {
        priorityNode<E> newNode = new priorityNode<>(paElement, paPriority);

        if (isShortFull) {
            if (paPriority >= aPmax) {
                if (isShortFull()) {
                    try {
                        priorityNode<E> pom = aShort.get(aShort.size() - 1);
                        aLong.add(pom);
                        aShort.add(pom);
                    } catch (EList ex) {
                        throw new EPriorityFront("Problem pushing to priorityFront caused by: " + ex.getMessage());
                    }
                } else {
                    addToShort(newNode);
                }
            } else {
                try {
                    aLong.add(newNode);
                } catch (EList ex) {
                    throw new EPriorityFront("Problem pushing to priorityFront caused by: " + ex.getMessage());
                }
            }
        } else {
            addToShort(newNode);
        }
        try {
            aPmax = aShort.get(aShort.size() - 1).getPriority();
        } catch (EList ex) {
            throw new EPriorityFront("Problem pushing to priorityFront caused by: " + ex.getMessage());
        }
        aCount++;

    }

    private boolean isShortFull() throws EPriorityFront {
        try {
            return this.aShort.size() > Math.sqrt(this.size());
        } catch (EList ex) {
            throw new EPriorityFront("Problem checking if priorityFront short List is full caused by: " + ex.getMessage());
        }
    }

    @Override
    public E pop() throws EPriorityFront {
        try {
            priorityNode<E> toDelete = aShort.delete(aShort.get(0));
            aCount--;
            if (aShort.size() == 0) {
                this.isShortFull = false;
                LinkedList<priorityNode<E>> buffer = aLong;
                aCount = 0;
                aLong = new LinkedList<>();
                for (priorityNode<E> ppf : buffer) {
                    push(ppf.getData(), ppf.getPriority());
                }
            }

            return toDelete.getData();

        } catch (EList ex) {
            throw new EPriorityFront("Problem poping from priorityFront caused by: " + ex.getMessage());
        }
    }

    @Override
    public E peek() throws EPriorityFront {
        try {
            return aShort.get(0).getData();
        } catch (EList ex) {
            throw new EPriorityFront("Problem peeking from priorityFront caused by: " + ex.getMessage());
        }
    }

    @Override
    public boolean isEmpty() throws EPriorityFront {

        return aCount == 0;

    }

    @Override
    public int size() throws EPriorityFront {

        return aCount;

    }

    @Override
    public void clear() throws EPriorityFront {

        aShort = new SortedLinkedList<>();
        aLong = new LinkedList<>();
        aPmax = 0;
        aCount = 0;
    }

    @Override
    public Iterator<E> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void addToShort(priorityNode<E> newNode) throws EPriorityFront {
        try {
            aShort.add(newNode);
            if (isShortFull()) {
                isShortFull = true;
            }
        } catch (EList ex) {
            throw new EPriorityFront("Problem pushing to priorityFront caused by: " + ex.getMessage());
        }
    }
}
