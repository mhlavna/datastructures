/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *//*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.priorityFronts.QueueUsingStrack;

import datastructures.fronts.EFront;
import datastructures.fronts.stack.Stack;
import datastructures.priorityFronts.EPriorityFront;
import datastructures.priorityFronts.IPriorityFront;
import java.util.Iterator;

/**
 *
 * @author Juraj
 */
public class QueueUsingStack<E> implements IPriorityFront<E> {

    private Stack stack;

    public QueueUsingStack() {
        this.stack = new Stack();
    }

    @Override
    public void push(E paElement, int paPriority) throws EPriorityFront {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public E pop() throws EPriorityFront {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public E peek() throws EPriorityFront {
        try {
            return (E) this.stack.peek();
        } catch (EFront ex) {
            throw new EPriorityFront(ex.getMessage());
        }
    }

    @Override
    public boolean isEmpty() throws EPriorityFront {
        try {
            return this.stack.isEmpty();
        } catch (EFront ex) {
            throw new EPriorityFront(ex.getMessage());
        }
    }

    @Override
    public int size() throws EPriorityFront {
        try {
            return this.stack.size();
        } catch (EFront ex) {
            throw new EPriorityFront(ex.getMessage());
        }
    }

    @Override
    public void clear() throws EPriorityFront {
        this.stack = new Stack();
    }

    @Override
    public Iterator<E> iterator() {
        return this.stack.iterator();
    }

    private int getParent(int paIndex) throws EPriorityFront {
        if (this.exists(paIndex)) {
            return paIndex / 2;
        } else {
            return -1;
        }
    }

    private int getLeftSon(int paIndex) throws EPriorityFront {
        if (this.exists(paIndex)) {
            return paIndex * 2;
        } else {
            return -1;
        }
    }

    private int getRightSon(int paIndex) throws EPriorityFront {
        if (this.exists(paIndex)) {
            return (paIndex * 2) + 1;
        } else {
            return -1;
        }
    }

    /**
     * Check if exists element with index paIndex
     *
     * @param paIndex
     * @return boolean
     * @throws EPriorityFront
     */
    private boolean exists(int paIndex) throws EPriorityFront {
        return paIndex > 0 && paIndex <= this.size();
    }
}
