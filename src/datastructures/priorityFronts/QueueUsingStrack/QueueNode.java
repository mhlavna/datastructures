/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.priorityFronts.QueueUsingStrack;

/**
 *
 * @author Juraj
 */
public class QueueNode<E> {

    private int priority;
    private E data;

    public QueueNode(int priority, E data) {
        this.priority = priority;
        this.data = data;
    }

    public int getPriority() {
        return priority;
    }

    public E getData() {
        return data;
    }

}
