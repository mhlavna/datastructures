/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.tables.bst;

import datastructures.tables.TablePair;

/**
 *
 * @author hlavna2
 */
public class BstNode<K, E> {

    private TablePair<K, E> aPair;
    private BstNode<K, E> parent;
    private BstNode<K, E> leftSon;
    private BstNode<K, E> rightSon;

    public BstNode(K paKey, E paElement) {
        aPair = new TablePair<>(paKey, paElement);

    }

    public BstNode<K, E> getLeftSon() {
        return leftSon;
    }

    public BstNode<K, E> getRightSon() {
        return rightSon;
    }

    public BstNode<K, E> getParent() {
        return parent;
    }

    public void setParent(BstNode<K, E> parent) {
        this.parent = parent;
    }

    public void setRightSon(BstNode<K, E> rightSon) {
        this.rightSon = rightSon;
    }

    public void setLeftSon(BstNode<K, E> leftSon) {
        this.leftSon = leftSon;
    }

    public TablePair<K, E> getPair() {
        return aPair;
    }

    public void setPair(TablePair<K, E> aPair) {
        this.aPair = aPair;
    }

    public K getKey() {
        return aPair.getKey();
    }

    public E getElement() {
        return aPair.getElement();
    }

    public void setElement(E paElemenet) {
        aPair.setElement(paElemenet);
    }

    public boolean hasLeft() {
        return leftSon != null;
    }

    public boolean hasRight() {
        return rightSon != null;
    }

    public boolean hasParent() {
        return parent != null;
    }

    public boolean isLeft() {
        if (!hasParent()) {
            return false;
        }
        if (this.parent.getLeftSon() == this) {
            return true;
        }
        return false;
    }

    public boolean isRight() {
        if (!hasParent()) {
            return false;
        }
        if (this.parent.getRightSon() == this) {
            return true;
        }
        return false;
    }
}
