/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.tables.bst;

import datastructures.tables.ETable;
import datastructures.tables.ITable;
import datastructures.tables.Sorts.KeyComparer;
import datastructures.tables.TablePair;
import java.util.Iterator;

/**
 *
 * @author hlavna2
 */
public class BinarySearchTree<K, E> implements ITable<K, E> {

    private BstNode<K, E> aRoot;
    private int aCount;
    private KeyComparer<K> aComparer;

    @Override
    public void insert(K paKey, E paElement) throws ETable {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public E delete(K paKey) throws ETable {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public E find(K paKey) throws ETable {
        BstNode<K, E> node = findNode(paKey, false);
        if (node == null) {
            return null;
        }
        return node.getElement();
    }

    @Override
    public void modify(K paKey, E paNewElement) throws ETable {
        BstNode<K, E> node = findNode(paKey, true);
        if (node == null) {
            throw new ETable("Error while modifying: Key not present ( " + paKey + ") ");
        }

        node.setElement(paNewElement);
    }

    @Override
    public boolean containsKey(K paKey) throws ETable {
        return findNode(paKey, false) != null;
    }

    @Override
    public boolean isEmpty() throws ETable {
        return aCount == 0;
    }

    @Override
    public int size() throws ETable {
        return this.aCount;
    }

    @Override
    public void clear() throws ETable {
        this.aCount = 0;
        this.aRoot = null;
    }

    @Override
    public Iterator<TablePair<K, E>> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private BstNode<K, E> findNode(K paKey, boolean paAlwaysReturnNode) {
        BstNode<K, E> node, nextNode;
        node = aRoot;
        while (node != null) {
            if (aComparer.areEqual(paKey, node.getKey())) {
                return node;
            }
            if (aComparer.firstIsGreater(paKey, node.getKey())) {
                nextNode = node.getRightSon();
                if (nextNode == null && paAlwaysReturnNode) {
                    return node;
                }
                node = nextNode;

            }

            if (aComparer.firstIsLess(paKey, node.getKey())) {
                nextNode = node.getLeftSon();
                if (nextNode == null && paAlwaysReturnNode) {
                    return node;
                }
                node = nextNode;
            }
        }

        return node;


    }

    public void linkLeft(BstNode<K, E> node, BstNode<K, E> parent) {
        parent.setLeftSon(node);
        node.setParent(parent);
    }

    public void linkRight(BstNode<K, E> node, BstNode<K, E> parent) {
        parent.setRightSon(node);
        node.setParent(parent);
    }

    public void unlinkParent(BstNode<K, E> node) {
        node.setParent(null);
    }
}
