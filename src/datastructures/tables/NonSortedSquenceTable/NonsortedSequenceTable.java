package datastructures.tables.NonSortedSquenceTable;

import datastructures.lists.ArrayList.ArrayList;
import datastructures.lists.EList;
import datastructures.tables.ETable;
import datastructures.tables.ITable;
import datastructures.tables.TablePair;
import java.util.Iterator;

/**
 * Implementation of the nonsorted sequence table
 *
 * @author Martin Hlavňa
 */
public class NonsortedSequenceTable<K, E> implements ITable<K, E> {

    protected ArrayList<TablePair<K, E>> aData;

    public NonsortedSequenceTable() {
        this.aData = new ArrayList<>();
    }

    @Override
    public void insert(K paKey, E paElement) throws ETable {
        if (containsKey(paKey)) {
            throw new ETable("Error while inserting element into table: Key already present(" + paKey + ")");
        }
        try {
            aData.add(new TablePair<>(paKey, paElement));
        } catch (EList ex) {
            throw new ETable("Error while inserting element into table caused by: " + ex.getMessage());
        }
    }

    @Override
    public E delete(K paKey) throws ETable {
        int index = indexOfKey(paKey);
        if (index > -1) {
            try {
                swap(index, size() - 1);
                return aData.deleteFromIndex(size() - 1).getElement();
            } catch (EList ex) {
                throw new ETable("Error while deleting from table caused by: " + ex.getMessage());
            }
        }
        throw new ETable("Error while deleting from table: Key not present(" + paKey + ")");
    }

    @Override
    public E find(K paKey) throws ETable {
        int index = indexOfKey(paKey);

        if (index > -1) {
            try {
                return aData.get(index).getElement();
            } catch (EList ex) {
                throw new ETable("Error while findig element by key caused by " + ex.getMessage());

            }
        }
        return null;
    }

    @Override
    public void modify(K paKey, E paNewElement) throws ETable {
        int index = indexOfKey(paKey);
        if (index > -1) {
            try {
                aData.get(index).setElement(paNewElement);
            } catch (EList ex) {
                throw new ETable("Error while modifying element by key caused by " + ex.getMessage());

            }
        } else {
            throw new ETable("Error while modifying element by key: No such key(" + paKey + ")");
        }

    }

    @Override
    public boolean containsKey(K paKey) throws ETable {
        return indexOfKey(paKey) > -1;
    }

    @Override
    public boolean isEmpty() throws ETable {
        return size() == 0;
    }

    @Override
    public int size() throws ETable {
        try {
            return aData.size();
        } catch (EList ex) {
            throw new ETable("Error while getting size of the table caused by: " + ex.getMessage());
        }
    }

    @Override
    public void clear() throws ETable {
        try {
            aData.clear();
        } catch (EList ex) {
            throw new ETable("Error while clearing table caused by: " + ex.getMessage());
        }
    }

    @Override
    public Iterator<TablePair<K, E>> iterator() {
        return aData.iterator();
    }

    /**
     * Searches data for key
     *
     * @param paKey Key to find
     * @return index of key if element is in table; -1 if element is not present
     * in table
     * @throws ETable if there is problem with underlaying structure
     */
    protected int indexOfKey(K paKey) throws ETable {
        try {
            for (int i = 0; i < size(); i++) {
                if (aData.get(i).getKey().equals(paKey)) { //TODO: Spraviť na nejaký compator
                    return i;
                }
            }
        } catch (EList ex) {
            throw new ETable("Error while getting index of key caused by: " + ex.getMessage());
        }
        return -1;
    }

    /**
     * Swaps 2 elements
     *
     * @param paFirst index of first element
     * @param paSecond index of second element
     * @throws ETable
     */
    private void swap(int paFirst, int paSecond) throws ETable {
        if (paFirst == paSecond) {
            return;
        }
        try {
            TablePair<K, E> tmp = aData.get(paFirst);
            aData.set(paFirst, aData.get(paSecond));
            aData.set(paSecond, tmp);
        } catch (EList ex) {
            throw new ETable("Swapping elements in table failed caused by: " + ex.getMessage());
        }
    }
}
