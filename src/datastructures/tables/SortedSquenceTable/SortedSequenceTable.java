/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.tables.SortedSquenceTable;

import datastructures.lists.EList;
import datastructures.tables.ETable;
import datastructures.tables.NonSortedSquenceTable.NonsortedSequenceTable;
import datastructures.tables.Sorts.KeyComparer;
import datastructures.tables.TablePair;

public class SortedSequenceTable<K, E> extends NonsortedSequenceTable<K, E> {

    protected KeyComparer<K> comparer;

    public SortedSequenceTable(KeyComparer<K> comparer) {
        super();
        this.comparer = comparer;
    }

    @Override
    protected int indexOfKey(K paKey) throws ETable {
        return bisectionSearch(paKey, size() - 1, 0, false);
    }

    @Override
    public void insert(K paKey, E paElement) throws ETable {
        try {
            if (isEmpty()) {
                aData.add(new TablePair<>(paKey, paElement));
                return;
            }

            int index = bisectionSearch(paKey, size() - 1, 0, true);
            int compare = comparer.compare(paKey, aData.get(index).getKey());

            if (compare < 0) {
                aData.insert(new TablePair<>(paKey, paElement), index);
            } else if (compare == 0) {
                throw new ETable("Error while inserting element into table: Key already present(" + paKey + ")");
            } else {
                aData.insert(new TablePair<>(paKey, paElement), index + 1);
            }
        } catch (EList ex) {
            throw new ETable("Insert into SortedSquenceTable failed caused by: " + ex.getMessage());
        }


    }

    @Override
    public E delete(K paKey) throws ETable {
        int index = indexOfKey(paKey);
        if (index > 0) {
            try {
                return aData.deleteFromIndex(index).getElement();
            } catch (EList ex) {
                throw new ETable("Deleting from SortedSequenceTable failed caused by: " + ex.getMessage());
            }
        }
        throw new ETable("Error while deleting from table: Key not present(" + paKey + ")");
    }

    /**
     * Performs search for key using bisection
     *
     * @param paKey Key to search for
     * @param paHigh High interval
     * @param paLow Low interval
     * @param returnLastIndex If method should return -1 or index of last
     * element
     * @return -1 if key was not present and foruth parameter was false, index
     * of element otherwise
     */
    private int bisectionSearch(K paKey, int paHigh, int paLow, boolean returnLastIndex) throws ETable {
        if (paHigh < paLow) {
            return -1;
        }

        int middle = (paHigh + paLow) / 2;

        while (paHigh != paLow) {
            try {
                middle = (paHigh + paLow) / 2;
                if (comparer.areEqual(paKey, aData.get(middle).getKey())) {
                    return middle;
                }
                if (comparer.firstIsLess(paKey, aData.get(middle).getKey())) {
                    paHigh = middle;
                } else {
                    paLow = middle + 1;
                }
            } catch (EList ex) {
                throw new ETable("Bisection searched failed, caused by: " + ex.getMessage());
            }

        }
        if (returnLastIndex) {
            return middle;
        }
        return -1;
    }
}
