/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.lists.ArrayList;

import datastructures.lists.EList;
import java.util.Iterator;

/**
 *
 * @author hlavna2
 */
public class ArrayListIterator<E> implements Iterator<E> {

    private ArrayList<E> aList;
    private int aIndex;

    public ArrayListIterator(ArrayList<E> paList) {
        this.aList = paList;
        this.aIndex = -1;
    }

    @Override
    public boolean hasNext() {
        try {
            return aIndex < aList.size() - 1;
        } catch (EList ex) {
            return false;
        }

    }

    @Override
    public E next() {
        try {
            return aList.get(++aIndex);
        } catch (EList ex) {
            return null;
        }
    }

    @Override
    public void remove() {
    }
}
