/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.lists.ArrayList;

import datastructures.lists.EList;
import datastructures.lists.IList;
import java.util.Iterator;

public class ArrayList<E> implements IList<E> {

    private E[] aElements;
    private int aCount;
    private static final int INITIAL_SIZE = 10;

    /**
     * Creates instance of ArrayList with default size of 10
     */
    public ArrayList() {
        aElements = (E[]) new Object[INITIAL_SIZE];
        aCount = 0;
    }

    /**
     * Creates instance of ArrayList with size determinded by parameter. If
     * length is negative then defualt length of 10 is used
     *
     * @param paLength length of array
     */
    public ArrayList(int paLength) {

        if (paLength <= 0) {
            paLength = INITIAL_SIZE;
        }
        this.aCount = 0;
        aElements = (E[]) new Object[paLength];
    }

    @Override
    public void add(E paElement) throws EList {
        if (aCount == aElements.length) {
            expandCapacity();
        }

        aElements[aCount++] = paElement;
    }

    @Override
    public void insert(E paElement, int paIndex) throws EList {
        if (!isValid(paIndex) && paIndex != aCount) {
            throw new EList("Specified index is out of bounds. Correct bounds are from 0 to " + (aCount));
        }


        E[] newElements;
        if (aCount == aElements.length) {
            newElements = (E[]) new Object[aCount + INITIAL_SIZE];
            System.arraycopy(aElements, 0, newElements, 0, aCount);
        } else {
            newElements = aElements;//(E[]) new Object[aElements.length];
        }
        //System.arraycopy(aElements, 0, newElements, 0, paIndex);
        System.arraycopy(aElements, paIndex, newElements, paIndex + 1, aCount - paIndex);
        newElements[paIndex] = paElement;
        //System.arraycopy(aElements, paIndex+1, newElements, paIndex + 1, aCount - paIndex);
        aElements = newElements;
        aCount++;
    }

    @Override
    public E delete(E paElement) throws EList {
        int index = indexOf(paElement);
        if (index == -1) {
            return null;
        }
        return deleteFromIndex(index);
    }

    @Override
    public E deleteFromIndex(int paIndex) throws EList {
        if (isValid(paIndex)) {
            E element = aElements[paIndex];
            System.arraycopy(aElements, paIndex + 1, aElements, paIndex, aCount - paIndex - 1);

            if (aCount + 10 == aElements.length) {
                E[] newElements = (E[]) new Object[aCount];
                System.arraycopy(aElements, 0, newElements, 0, aCount - 1);
                aElements = newElements;
            }
            aElements[--aCount] = null;
            return element;

        } /*else if (paIndex == aCount) {
         //vyriešenie oživením, test je písaný neviem prečo na incluzívny delete
         return deleteFromIndex(paIndex - 1);
         }*/ else {
            throw new EList("Specified index is out of bounds. Correct bounds are from 0 to " + (aCount - 1));
        }
    }

    @Override
    public int indexOf(E paElement) throws EList {
        for (int i = 0; i < aCount; i++) {
            if (aElements[i] == paElement) { //ak ide o ten isty element, teda sa rovnaju pointre
                return i;
            }
        }
        return -1;
    }

    /**
     * Returns the first element that equals with parameter
     *
     * @param paElement
     * @return index of first element that equals with parameter, if no elements
     * equals with parameter then -1 is returned
     * @throws EList
     */
    public int indexOfEquals(E paElement) throws EList {
        for (int i = 0; i < aCount; i++) {
            if (aElements[i].equals(paElement)) { //ak ide o taky isty element, teda sa rovnaju vlastnosti
                return i;
            }
        }
        return -1;
    }

    /**
     * Returns the last index of element that equals with parameter
     *
     * @param paElement
     * @return last index of element that equals with parameter, if no element
     * matches the parameter then -1 is returned
     * @throws EList
     */
    public int lastIndexOf(E paElement) throws EList {
        for (int i = aCount - 1; i >= 0; i--) { //zacinam odzadu
            if (aElements[i] == paElement) { //ak ide o ten isty element, teda sa rovnaju pointre
                return i;
            }
        }
        return -1;
    }

    /**
     * Returns the last index of element, tests for quality
     *
     * @param paElement
     * @return Last index of element, if no element matches the parameter then
     * -1 is returned
     * @throws EList
     */
    public int lastIndexOfEquals(E paElement) throws EList {
        for (int i = aCount - 1; i >= 0; i--) { //zacinam odzadu
            if (aElements[i].equals(paElement)) { //ak ide o ten isty element, teda sa rovnaju pointre
                return i;
            }
        }
        return -1;
    }

    @Override
    public E get(int paIndex) throws EList {
        if (isValid(paIndex)) {
            return aElements[paIndex];
        }
        throw new EList("Specified index is out of bounds. Correct bounds are from 0 to " + (aCount - 1));
    }

    @Override
    public void set(int paIndex, E paElement) throws EList {
        if (isValid(paIndex) || paIndex == aCount) {
            if (aCount == aElements.length && paIndex == aCount) {
                expandCapacity();
            }
            aElements[paIndex] = paElement;
        } else {
            throw new EList("Specified index is out of bounds. Correct bounds are from 0 to " + (aCount));
        }

    }

    @Override
    public int size() throws EList {
        return aCount;
    }

    @Override
    public void clear() throws EList {
        aElements = (E[]) new Object[INITIAL_SIZE];
        aCount = 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayListIterator<>(this);
    }

    /**
     * Checks if index is in bounds
     *
     * @param paIndex tested index
     * @return true if index is in bounds
     */
    private boolean isValid(int paIndex) {
        return paIndex >= 0 && paIndex < aCount;
    }

    /**
     * Expands capacity of array
     */
    private void expandCapacity() {
        E[] newElements = (E[]) new Object[aCount + INITIAL_SIZE];
        System.arraycopy(aElements, 0, newElements, 0, aCount);
        aElements = newElements;
    }
}
