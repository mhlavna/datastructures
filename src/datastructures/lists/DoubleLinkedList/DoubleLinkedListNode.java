/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.lists.DoubleLinkedList;

import datastructures.lists.LinkedList.LinkedListChunk;

/**
 *
 * @author ikim23
 */
public class DoubleLinkedListNode<E> extends LinkedListChunk<E> {

    private DoubleLinkedListNode<E> aPrevious;

    public DoubleLinkedListNode(E data) {
        super(data);
        aPrevious = null;
    }

    public DoubleLinkedListNode<E> getPrevious() {
        return aPrevious;
    }

    public void setPrevious(DoubleLinkedListNode<E> previous) {
        aPrevious = previous;
    }
}
