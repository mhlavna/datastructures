/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.lists.DoubleLinkedList;

import datastructures.lists.EList;
import datastructures.lists.LinkedList.LinkedList;
import datastructures.lists.LinkedList.LinkedListChunk;

/**
 *
 * @author Martin Hlavňa
 */
public class DoubleLinkedList<E> extends LinkedList<E> {

    public DoubleLinkedList() {
        super();
    }

    @Override
    public void add(E paElement) throws EList {
        if (aCount == 0) {
            aFirstChunk = new DoubleLinkedListNode<>(paElement);
            aLastChunk = aFirstChunk;
        } else {
            DoubleLinkedListNode<E> node = new DoubleLinkedListNode<>(paElement);
            node.setPrevious((DoubleLinkedListNode<E>) aLastChunk);
            aLastChunk.setNext(node);
            aLastChunk = aLastChunk.getNext();
        }
        aCount++;
    }

    @Override
    public void insert(E paElement, int paIndex) throws EList {
        if (isValid(paIndex)) {
            DoubleLinkedListNode<E> inserted;
            if (paIndex == 0) {
                inserted = new DoubleLinkedListNode<>(paElement);
                inserted.setNext(aFirstChunk);
                ((DoubleLinkedListNode<E>) aFirstChunk).setPrevious(inserted);
                aFirstChunk = inserted;
            } else {
                DoubleLinkedListNode<E> oldNode = (DoubleLinkedListNode<E>) getChunkAtIndex(paIndex);
                DoubleLinkedListNode<E> node = new DoubleLinkedListNode<>(paElement);
                node.setNext(oldNode);
                node.setPrevious(oldNode.getPrevious());
                oldNode.getPrevious().setNext(node);
                oldNode.setPrevious(node);
            }
            aCount++;
        } else {
            throw new EList("Invalid index!");
        }
    }

    @Override
    public E deleteFromIndex(int paIndex) throws EList {
        if (isValid(paIndex)) {
            E value;
            if (paIndex == 0) {
                value = aFirstChunk.getData();
                aFirstChunk = aFirstChunk.getNext();
                ((DoubleLinkedListNode<E>) aFirstChunk).setPrevious(null);
            } else if (paIndex == aCount - 1) {
                value = aLastChunk.getData();
                aLastChunk = ((DoubleLinkedListNode<E>) aLastChunk).getPrevious();
                aLastChunk.setNext(null);
            } else {
                DoubleLinkedListNode<E> deleted = (DoubleLinkedListNode<E>) getChunkAtIndex(paIndex);
                value = deleted.getData();
                deleted.getPrevious().setNext(deleted.getNext());
                ((DoubleLinkedListNode<E>) deleted.getNext()).setPrevious(deleted.getPrevious());
            }
            aCount--;
            return value;
        } else {
            throw new EList("Invalid index!");
        }
    }

    @Override
    protected LinkedListChunk<E> getChunkAtIndex(int index) throws EList {
        if (isValid(index)) {
            if (index < aCount - index) {
                return super.getChunkAtIndex(index);
            } else {
                DoubleLinkedListNode<E> node = (DoubleLinkedListNode<E>) aLastChunk;
                for (int i = aCount - 1; i > index; i--) {
                    node = node.getPrevious();
                }
                return node;
            }
        } else {
            throw new EList("Wrong index.");
        }
    }

    private boolean isValid(int paIndex) {
        return paIndex >= 0 && paIndex < aCount;
    }
}
