/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.lists.LinkedList;

import java.util.Iterator;

/**
 *
 * @author hlavna2
 */
public class LinkedListIterator<E> implements Iterator<E> {
    private LinkedList<E> list;
    private LinkedListChunk current;
    /**
     * Creates new Linked List
     * @param list 
     */
    public LinkedListIterator(LinkedList list) {
        this.current = list.aFirstChunk;
        this.list = list;
    }
    
    
    @Override
    public boolean hasNext() {
        if(current != null){
            return true;
        }
        return false;
    }

    @Override
    public E next() {
        E output = (E) current.getData();
        current = current.getNext();
        return output;
        
    }

    @Override
    public void remove() {
        
    }

    
}
