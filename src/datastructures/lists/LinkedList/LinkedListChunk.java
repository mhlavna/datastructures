/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.lists.LinkedList;

/**
 *
 * @author hlavna2
 */
public class LinkedListChunk<E> {

    private E data;
    private LinkedListChunk<E> next;

    /**
     * Initialized chunk, with data
     *
     * @param data Data to intialize with
     */
    public LinkedListChunk(E data) {
        this.data = data;
        this.next = null;
    }

    /**
     * Returns data object from chunk
     *
     * @return data object of type E
     */
    public E getData() {
        return data;
    }

    /**
     * Return next chunk
     *
     * @return Next chunk
     */
    public LinkedListChunk<E> getNext() {
        return next;
    }

    /**
     * Sets next chunk
     *
     * @param next Next chunk
     */
    public void setNext(LinkedListChunk<E> next) {
        this.next = next;
    }

    /**
     * Sets data
     *
     * @param data Data object
     */
    public void setData(E data) {
        this.data = data;
    }
}
